import React from 'react';

class App extends React.Component {
   constructor() {
      super();
      this.state = {
        error: null,
        maxTime:0,
        isLoaded: false,
        items:{"bidPrice":"","askPrice":"","marketPrice":"","maxTime":""}, 
        data: 
         [
            {
               "id":1,
               "name":"Foo",
               "age":"20"
            },
            {
               "id":2,
               "name":"Bar",
               "age":"30"
            },
            {
               "id":3,
               "name":"Baz",
               "age":"40"
            }
         ]
      };
      this.fetchPrice = this.fetchPrice.bind(this);
   }

   
fetchPrice() {
   console.log('fetch price menthod')
   fetch("/process")
     .then(res => res.json())
     .then(
       (result) => {
         console.log('result component did mount -->> ',result)
         this.setState({
           isLoaded: true,
           items: result,
           maxTime: this.state.maxTime+1
         });
       },
       // Note: it's important to handle errors here
       // instead of a catch() block so that we don't swallow
       // exceptions from actual bugs in components.
       (error) => {
         this.setState({
           isLoaded: true,
           error
         });
       }
     )
}


      intervalID;
      

      componentDidMount() {
        this.fetchPrice();
        this.setState({
         maxTime: this.state.maxTime+1
       });

       console.log('maxtime   ',this.state.maxTime);
         if(this.state.maxTime<10) {
         console.log('maxtime   ',this.state.maxTime);
            this.intervalID = setInterval(this.fetchPrice.bind(this), 1000);
         } else {
            console.log('else part');
         }

      }

      componentWillUnmount() {
         this.setState({
            maxTime: this.state.maxTime+1
          });
   
         clearInterval(this.intervalID);
      }
  
   render() {
      return (
         <div>
            <Header/>
            {this.state.items.askPrice} --- {this.state.items.bidPrice}
         </div>
      );
   }
}

class Header extends React.Component {
   render() {
      return (
         <div>
            <h1>Header</h1>
         </div>
      );
   }
}
class TableRow extends React.Component {
   render() {
      return (
         <tr>
            <td>{this.props.data.id}</td>
            <td>{this.props.data.name}</td>
            <td>{this.props.data.age}</td>
         </tr>
      );
   }
}
export default App;