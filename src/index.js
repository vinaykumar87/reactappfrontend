import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

const element = <React.StrictMode><App /></React.StrictMode>;
const name = 'Josh Perez';
const user = {firstName:'Josh',lastName:'Perez s'};

function formatName(user) {
  console.log('formating name..'+JSON.stringify(user));
  return user.firstName+ ' '+user.lastName;
}

const tic = <div><h2>today date {new Date().toTimeString()}.</h2></div>
//const element = <h1>Hello, {formatName(user)} It is ::{tick()}</h1>;

function tick() {
  const element = (
    <div>
      <h1>Hello, world!</h1>
      <h2>It is {new Date().toLocaleTimeString()}.</h2>
    </div>
  );
}

ReactDOM.render(element, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
