import React from 'react';
import './App.css';
class App extends React.Component {
   constructor() {
      super();
      this.state = {
        error: null,
        isLoaded: false,
        askArrow:[false],
        bidArrow:[false],
        items: [{'askPrice':0,'bidPrice':0}], 
      };
      this.fetchPrice = this.fetchPrice.bind(this);
   }

   
fetchPrice() {
   console.log('fetch price menthod')
   let item = this.state.items;
   fetch("/process")
     .then(res => res.json())
     .then(
       (result) => {
          let bidArrow1 =  new Array(); ;
          let askArrow1 =  new Array(); ;

          for (let i = 0; i < item.length; i++) {
             if(item[i].bidPrice>result[i].bidPrice) {
               bidArrow1.push(true); 
             } else {
               bidArrow1.push(false);
             }
             if(item[i].askPrice>result[i].askPrice) {
               askArrow1.push(true);
            } else {
               askArrow1.push(false);
            }
            
            console.log('bidArrow1 values`',bidArrow1);
          }
         this.setState({
           isLoaded: true,
           items: result,
           bidArrow: bidArrow1,
           askArrow: askArrow1
         });
         console.log('setting result to items ',this.state.items);
       },
       (error) => {
         this.setState({
           isLoaded: true,
           error
         });
       }
     )
}
      intervalID;
      componentDidMount() {
        this.fetchPrice();
      this.intervalID = setInterval(this.fetchPrice.bind(this), 5000);
      }

      componentWillUnmount() {   
         clearInterval(this.intervalID);
      }
   render() {
      console.log('this.state.askArrow in render',this.state.askArrow);
      return (
         <div>
<section>
  <h1>Trading Application</h1>
  <div class="tbl-header">
    <table cellpadding="0" cellspacing="0" border="0">
      <thead>
        <tr>
          <th>Code</th>
          <th>Company</th>
          <th>Bid PRice</th>
          <th>Ask Price</th>
        </tr>
      </thead>
    </table>
  </div>
  <div class="tbl-content">
    <table cellpadding="0" cellspacing="0" border="0">
      <tbody>
         
      {this.state.items.map((person, i) => 
         <tr>
         <td>{person.code}</td>
         <td>{person.company}</td>
      <td>{person.bidPrice} {this.state.bidArrow[i]?<DownArrow/>:<UpArrow/>}</td>
         <td>{person.askPrice} {this.state.askArrow[i]?<DownArrow/>:<UpArrow/>}</td>
       </tr>
         )}   
      </tbody>
    </table>
  </div>
</section>
</div>
      );
   }
}


class UpArrow extends React.Component {
   render() {
      return (
         <i class='arrowgreen up'></i>
      );
   }
} 
class DownArrow extends React.Component {
   render() {
      return (
         <i class='arrowred down'></i>
      );
   }
}
export default App;